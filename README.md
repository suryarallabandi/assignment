# Assignment
Branches In Gitlab
    - Master
    - UAT
    - Stage
    - Prod
1) Triggering CI through Webhook
    Install "Gitlab plugin" on jenkins & restart jenkins
    Generate personal access token in gitlab
    In jenkins add credentials for gitlab API token with the above value
    Once the api access has been setup, we can configure the connection between jenkins and gitlab in configure system
    Now create webhook in gitlab project - settings - webhook
2) SCM Checkout
    pre-check- Evaluate whether to build the code or not, depending on the files modified in the commit
3) Build Artifacts
    Build artifacts using maven and run unit tests
4) Code Coverage
    Add the following maven jacoco plugin in pom.xml of the project
<plugin>
<groupId>org.jacoco</groupId>
<artifactId>jacoco-maven-plugin</artifactId>
<version>0.5.5.201112152213</version>
<configuration>
<destFile>${basedir}/target/coverage-reports/jacoco-unit.exec</destFile>
<dataFile>${basedir}/target/coverage-reports/jacoco-unit.exec</dataFile>
</configuration>
<executions>
<execution>
<id>jacoco-initialize</id>
<goals>
<goal>prepare-agent</goal>
</goals>
</execution>
<execution>
<id>jacoco-site</id>
<phase>package</phase>
<goals>
<goal>report</goal>
</goals>
</execution>
</executions>
</plugin>
    
    Invoke jacoco plugin directly
    $ mvn org.jacoco:jacoco-maven-plugin:0.5.5.201112152213:prepare-agent
5) Code Analysis
    Install sonarqube using docker image $ docker run -d --name sonarqube -p 9000:9000 sonarqube
    Install "sonarqube plugin" in jenkins
    configure sonarqube in configure system in jenkins
    Create "sonar-project.properties" file under the parent dir of the project & add below
 sonar.projectKey=SURYA
 sonar.projectName=SonarDemo
 sonar.projectVersion=1.0
 sonar.sources=src
 sonar.java.binaries=target\\main
 sonar.jacoco.reportPath=target\\coverage-reports\\jacoco-unit.exec

6) Quality Gates
    Create sonarqube user token in sonarqube server
    configure sonarqube webhook for quality gate in sonarqube 
    Install "Quality gates" plugin in jenkins
    add sonarqube token to jenkins in configure system
6) Archieve Artifacts
    Install Jfrog artifactory using docker
    Install artifactory plugin
    add server details in jenkins configure system
7) Build image
    Add docker hub credentials in manage credentials in jenkins
8) Deploy
    install docker compose on pilot machine $ apt install -y docker-compose
    Install "docker compose build step" plugin in jenkins
9) Smoke testsDe
    Check for application endpoint url
    check for core logs
10) Deploy using kubernetes
        Install "Kubernetes continuous depploy" plugin in jenkins
    Setup kubernetes cluster with master & Nodes

    Kubernetes Master Setup
Install Docker
    apt update && apt install -y docker.io
Add repo for kubernetes
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add-
    cat << EOF > /etc/apt/sources.list.d/kubernetes.list  deb http://apt.kubernetes.io/ kubernetes-xenial main EOF
Install Kubernetes Components
    apt update && apt install -y kubelet kubeadm kubectl
Now Initialize the cluster using the IP range for Flannel
    kubeadm init --pod-network-cidr=10.244.0.0/16
whatever ports you see in output, try to add in inbound security groups
    (Copy the output i.e. kubeadm join token, we will need this later)
Run following as a Non-root user
To start using the cluster, we need to run the following as a regular user i.e. ubuntu user
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
Now kubectl commands will work as non-root user as well
We should now deploy a pod network to the cluster
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
Thats it, now master node is up & running

Kubernetes Node Setup
Install Docker
    apt update && apt install -y docker.io
    Add repo for kubernetes
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add-
    cat << EOF > /etc/apt/sources.list.d/kubernetes.list  deb http://apt.kubernetes.io/ kubernetes-xenial main EOF
Install Kubernetes Components
    apt update && apt install -y kubeadm
Now join the cluster by running the output cmd obtained from 'kubeadm init' on master as root
Kubeadm Setup Completed

11) Helm installation & how to store helm package in s3 bucket
    #Install AWS CLI

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o"awscliv2.zip"
apt install unzip && unzip awscliv2.zip
ls
cd aws
apt ./aws/install --bin-dir/usr/bin --install-dir/usr/bin/aws-cli --update
aws --version
#Configure
aws configure

Access Key: 
Secret Access: 

#Install helm s3 plugin
helm plugin install https://github.com/hypnoglow/helm-s3.git

#Create s3 bucket with name "registo" (Name is something suitable for your app)
#Create helm repo on the above s3 bucket
helm s3 init s3://my-charts/charts
You need to add users in AWS IAM console - https://console.aws.amazon.com/

#Create Helm Package & upload to registry

helm repo add my-charts s3://my-charts/charts

helm package --version 1.0

helm s3 push ./<packageName> my-charts



#To install chart

helm search repo <AppName> --versions

helm install my-charts/<AppName>

helm repo list

helm get all <chartName>/<ReleaseName>

12) EFK Stack
What is EFK stack?

    It is a centralized log management setup for kubernetes cluster

What is EFK stands for?

    ElasticSearch, FluentD, Kibana

What is ElasticSearch, FluentD and Kibana?

    FluentD - It is a Data Collector

    ElasticSearch - It is used for search, analytics, storing

    Kibana - Data visualisation dashboard 

So, FluentD will collect data from nodes whereas ElasticSearch will fetch the data and analyze it and stores data in key pairs & Kibana will display all logs visually

Okay, next lets see with what objects we are defining this stack

FluentD         - DaemonSet

ElasticSearch - StatefulSet

Kibana          - Deployment

why we need to use these objects, please read carefully

we need to run FluentD in each and every node, so DaemonSet has the ability where it runs in every node in cluster
ElasticSearch will store so much of data in memory, so we use StaefulSet.... As it is a stateful application
we use Deployment obj for kibana, because we need to access it anywhere outside the cluster
Now, we will setup EFK stack

Setting up EFK is very easy as there is already helm charts available for that, but understanding the structure of how to setup is more important

we will discuss step by step 
#Setup EFK - clone this git repo
git clone https://github.com/cdwv/efk-stack-helm

#Go inside the folder
cd efk-stack-helm

#Edit values.yml & set the below values for rbac, kibana service type
rbac:
    enabled: true
service:
    type: NodePort

#Edit Chart.yml & add below line
version: 0.0.1
#Edit "/templates/kibana-deployment.yaml" & change apiVersion to apps/v1

#Now install the package 

#We have successfully deployed EFK STACK

Now what is the most important is, we should understand the structure of helm charts & also how to develop the template manifests

The EFK stack Helm Charts looks like this:::- mentioning only the important files here
/efk-stack-helm
Chart.yaml
values.yaml
/templates
fluentd-daemonset.yaml
fluentd-configmap.yaml
elasticsearch-statefulset.yaml
elasticsearch-service.yaml
kibana-deployment.yaml
kibana-service.yaml
FluentD
DaemonSet
Service
Elastic Search
StatefulSet
Service
Kibana
Deployment
Service
Now we will see the manifests one by one and understand how to define

We will see what is "rbac" first

Role-based access control (RBAC) is a method of regulating access to computer or network resources based on the roles of individual users within your organization.
If {{- if .Values.rbac.enabled -} means in the values.yaml, it should be enabled
so that we can use ClusterRole & ClusterRoleBinding in daemonset, deployment, statefulset before defining these objects 

Now we will see kibana-deployment.yaml
for image: "{{ .Values.kibana.image.repository }}:{{ .Values.kibana.image.tag }}"  as we can see in the values.yaml, the repository and tag defined seperately
for this we need to understand Golang scripting
In resources
{{ toYaml .Values.kibana.resources | indent 12 }} what this means??
we are actually saying that whatever is there inside resources in values.yaml, we want the entire thing (eg: limits, cpu, requests, cpu)
And also we are indenting spaces before those (limits, cpu, requests, cpu)
And the  next thing 
{{- with .Values.kibana.nodeSelector }}
nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
whatever you add inside nodeSelector: {how many sources you add}, it will reflect here 


Now we will see fluentd-daemonset

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: {{ template "fullname.fluentdElasticsearch" . }}
  labels:
    app: {{ template "name.fluentdElasticsearch" . }}
spec:
  selector:
    matchLabels:
      app: {{ template "name.fluentdElasticsearch" . }}
  template:
    metadata:
      labels:
        app: {{ template "name.fluentdElasticsearch" . }}
    spec:
      priorityClassName: system-node-critical #priorityClassName: high-priority
      #Pods can have priority. Priority indicates the importance of a Pod relative to other Pods.
      containers:
      - name: {{ template "fullname.fluentdElasticsearch" . }}
        image: "{{ .Values.fluentdElasticsearch.image.repository }}:{{ .Values.fluentdElasticsearch.image.tag }}"
        imagePullPolicy: {{ .Values.fluentdElasticsearch.image.pullPolicy }}
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: {{ .Values.fluentdElasticsearch.dockerContainersPath }}
          readOnly: true
        - name: config-volume
          mountPath: /etc/fluent/config.d
        resources:
{{ toYaml .Values.fluentdElasticsearch.resources | indent 12 }}
    {{- with .Values.fluentdElasticsearch.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.fluentdElasticsearch.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.fluentdElasticsearch.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: {{ .Values.fluentdElasticsearch.dockerContainersPath }}
      - name: config-volume
        configMap:
          name: {{ template "fullname.fluentdElasticsearch" . }}-config
{{- end }}

Priority classes
It is nothing but we give priorities to objects when to run

Containers data in /var/lib/docker/containers

ElasticSearch-StatefulSet

We are defining an emptyDir: {} and we store data, also we are defining an init container (optional)

Please go through each and every line, just get a clarity about this

And we will move to Helm Mini Project with Real-time log management
#Create a helm chart

I added my own application image & deployed

I added 2 replicas in values.yaml

Container creation takes time

Here we can see the events

container created and started

Now we can able to check our application running or Not ... Manually

Application is up & running

Now we need to open kibana dashboard and configure it

13) Monitoring Setup using prometheus & Grafana

#we need to add these 2 repo's to the list

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts


helm repo add stable https://kubernetes-charts.storage.googleapis.com/

Note:: I Have added these 2 repo's in "execute.sh" itself

#Check whether added or not
helm repo list

#update once to refresh everything 
helm repo update

#Untar the stack
helm pull --untar prometheus-community/kube-prometheus-stack
Note:: i have added this in "execute.sh"

Now your kube-prometheus-stack installed successfully

Now we need to make some changes

First of all we need to remove all metrics related files & dependencies (we will install it seperately)

Now go inside "charts" 
    in that, go inside "grafana"
        edit "values.yaml"
Change the values like this 

type: NodePort
port: 3000

Now go to the original values.yaml - "vi /kube-prometheus-stack/values.yaml"

Search for "Configuration for prometheus service" under set this values

type: NodePort

Now install "helm install myprom ."

Successfully deployed

Now we need to install metrics server, for that follow the steps carefully

#run this for Metrics
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.7/components.yaml

kubectl logs -n kube-system <Name>

Now we can see that metrics server is installed, but not running'

For this, what we need to do is 

We can see the error like this, when we run logs
kubectl logs -n kube-system <Name>

To rectify this, please follow the steps carefully 

#Edit file and make changes
kubectl edit deploy -n kube-system metrics-server

#and add the following under .spec.template.spec.containers
command:
    - /metrics-server
    - --kubelet-insecure-tls
Now the metrics will work

we can check with these commands

kubectl top pod
kubectl top pod --namespace=<namespace_name>
kubectl top pod --namespace=kube-system

Success!

Now 
    kubectl get all
Now we can access purely integrated "Prometheus & Grafana"

