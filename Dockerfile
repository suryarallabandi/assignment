#==== Base Image =====
#ARG BASE_IMAGE_NAME
#FROM ${BASE_IMAGE_NAME}
#ENV WILDFLY_VERSION 19.0.0.Final
#ENV WILDFLY_SHA1 0d47c0e8054353f3e2749c11214eab5bc7d78a14
#ENV JBOSS_HOME /opt/jboss/wildfly
#USER root
#RUN mkdir /var/log/surya
#RUN chown jboss:jboss /var/log/surya
#RUN cd $HOME \
#    && curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
#    && sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1 \
#    && tar xf wildfly-$WILDFLY_VERSION.tar.gz \
#    && mv $HOME/wildfly-$WILDFLY_VERSION $JBOSS_HOME \
#    && rm wildfly-$WILDFLY_VERSION.tar.gz \
#    && chown -R jboss:0 ${JBOSS_HOME} \
#    && chmod -R g+rw ${JBOSS_HOME}
#ENV LAUNCH_JBOSS_IN_BACKGROUND true
#USER jboss
#EXPOSE 8080
#CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
#I have already build the base image and pushed to my docker hub repo

#jboss/wildfly
FROM suryarall/devopsprojectone
ADD sampleproject/target/sampleproject.jar /opt/jboss/wildfly/standalone/deployments/
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
